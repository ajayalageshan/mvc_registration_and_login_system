$.validator.addMethod('validPassword',
  function (value, element, param) {
    if (value != '') {
      if (value.match(/.*[a-z]+.*/i) == null) {
        return false;
      }
      if (value.match(/.*\d+.*/) == null) {
        return false;
      }
    }

    return true;
  },
  'Must contain at least one letter and one number'
);//79 moved repeated validation script to this file rom new.html and reset.html
